import os, sys
import io_tools

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans

import pylab as plt

proposal_csv_file = './proposals.tsv'

def getUniqueElementCounts(data, key):
    string_dict = {}
    for d in data:
        if d.get(key) not in [None, '', 'None','null']:
            if not string_dict.get(d[key]):
                string_dict[d[key]] = 1
            else:
                string_dict[d[key]] += 1
    return string_dict

def getUniqueRejections(data):
    string_dict = {}
    for d in data:
        if d['rejected_message'] != 'None' and d['rejected_message'] != 'null':
            #strings.append(d['rejected_message'])
            if not string_dict.get(d['rejected_message']):
                string_dict[d['rejected_message']] = 1
            else:
                string_dict[d['rejected_message']] += 1
    return string_dict

def cluster(vectors, strings, clusters=20):
    km = KMeans(n_clusters=clusters, 
                init='k-means++', 
                max_iter=100, 
                n_init=17,
                verbose=True)
    km.fit(vectors)
    labels = km.labels_

    results = {}
    for i in range(len(strings)):
        if not results.get(labels[i]):
            results[labels[i]] = []
        string = list(strings.keys())[i]
        count_value = strings[string]
        results[labels[i]].append((string,count_value))
    return results

def woResults(results):
    with open('results_scikit.csv','w') as f:
        f.write("grp,msg_cnt,message\n")
        for key, values in results.items():
            for value in values:
                f.write('%i,%i,"%s"\n'%(key,value[1],value[0]))

def main(proposal_csv_file):
    keys, data = io_tools.getCsvReader(proposal_csv_file, '\t')

    unique_rejections = getUniqueRejections(data)

    vectorizer = TfidfVectorizer(max_df=0.5, 
                                 max_features=1000,
                                 min_df=2, 
                                 stop_words=None,
                                 ngram_range = (2,2),
                                 use_idf=True)
    vectors = vectorizer.fit_transform(list(unique_rejections.keys())) # list of strings
    
    results = cluster(vectors, unique_rejections, clusters=30)
    woResults(results)

def getPieData(data, target_key):
    colors = []
    elements = getUniqueElementCounts(data, target_key)
    labels = list(elements.keys())
    
    total = sum(elements.values())
    sizes = [float(v)/total for v in elements.values()]
    colors = [plt.cm.Paired(s) for s in sizes]
 
    return labels, sizes, colors

def filterData(data, filter_element, key):
    new = []
    for d in data:
        if d[key] == filter_element:
            new.append(d)
    return new

def analyze(csv_file, proposal_csv_file):
    '''
    extract the clustering results from csv_file
    add the cluster label dimension to proposal_csv_file
    '''

    result_keys, result_data = io_tools.getCsvReader(csv_file, ',')
    l_key = result_keys[0]
    s_key = result_keys[3]
    prop_s_key = 'rejected_message'
    prop_l_key = 'rejected_category'

    str2label = {}
    for d in result_data:
        if d[l_key] == '':
            d[l_key] = 'altres'
        str2label[d[s_key]] = d[l_key] #strings are unique, we are not worried about overwriting

    #print(len(list(str2label.keys())))
    keys, data = io_tools.getCsvReader(proposal_csv_file, '\t')
    for d in data:
        if d[prop_s_key] != 'null' and d[prop_s_key] != 'None':
            d[prop_l_key] = str2label[d[prop_s_key]]
        else:
            d[prop_l_key] = '' #for consistency

    io_tools.outCsv(keys+[prop_l_key], data, 'proposal_enriched.csv')

    labels, sizes, colors_total = getPieData(data, prop_l_key)

    plt.axes(aspect=1)
    '''
    plt.pie(sizes, labels=labels, colors=colors_total,
            autopct='%1.1f%%', shadow=False, startangle=90)
    plt.savefig('./graphs/dist_total.png')
    plt.clf()
    '''

    filter_key = 'district__name'
    filter_elements = getUniqueElementCounts(data,filter_key)
    i = 0
    print(filter_elements, len(filter_elements.keys()))
    for filter_element in filter_elements.keys():
        plt.axes(aspect=1)
        plt.title('Rejection categories for %s'%filter_element,fontsize=14)
        data_filtered = filterData(data, filter_element, filter_key)
        labels, sizes, colors = getPieData(data_filtered, prop_l_key)
        plt.pie(sizes, labels=labels, colors=colors_total,
                autopct='%1.1f%%', shadow=False, startangle=90)
        plt.savefig('./graphs/rejection_cat_%s.png'%filter_element)
        plt.clf()

if __name__ == "__main__":
    main(proposal_csv_file)
    #analyze('results_scikit03.csv', proposal_csv_file)

