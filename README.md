# Ngram Clustering for #Metadecidim

The algorithm categorizes the rejections messages of the results of the PAM, using `scilearn` module. The process and the data are described [here](http://collectivat.cat/en/2017/01/08/data-auditing-decidim/).

The script uses python3 and the rest is in the requirements.txt, and the data comes from [here](https://github.com/elaragon/metadecidim)
